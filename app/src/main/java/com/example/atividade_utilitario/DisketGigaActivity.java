package com.example.atividade_utilitario;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DisketGigaActivity extends AppCompatActivity {
    private static final BigDecimal capacidadeDisket = new BigDecimal(1.84);
    private Button btnCalcular;
    private BigDecimal gigaPendrive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disket_giga);

        btnCalcular = findViewById(R.id.btn_disket_giga);
        btnCalcular.setOnClickListener(o -> {
            EditText text = findViewById(R.id.giga_pen_drive);
            gigaPendrive = new BigDecimal((text.getText().toString()));

            Toast.makeText(DisketGigaActivity.this,gigaPendrive.multiply(BigDecimal.valueOf(1024)).divide(capacidadeDisket,2 ,RoundingMode.HALF_DOWN)
                    .toString() + " Diskets ", Toast.LENGTH_LONG).show();

        });


    }
}
