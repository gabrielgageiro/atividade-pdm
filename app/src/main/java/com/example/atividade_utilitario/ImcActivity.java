package com.example.atividade_utilitario;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ImcActivity extends AppCompatActivity {

    Button btnCalcular;
    Button btnLimpar;
    BigDecimal peso;
    BigDecimal altura;
    BigDecimal pesoIdeal;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);
        this.getSupportActionBar().setSubtitle("Uma medida de gordura corporal em adultos");


        btnCalcular = findViewById(R.id.btn_calcular);
        btnCalcular.setOnClickListener(view -> {
            EditText text = findViewById(R.id.peso);
            peso = new BigDecimal((text.getText().toString()));
            text = findViewById(R.id.altura);
            altura = new BigDecimal((text.getText().toString()));
            BigDecimal alturaCm = altura.multiply(BigDecimal.valueOf(100));
            pesoIdeal = alturaCm.subtract(BigDecimal.valueOf(100))
                    .subtract((alturaCm.subtract(peso))
                    .divide(BigDecimal.valueOf(4))
                    .multiply(BigDecimal.valueOf(5)
                    .divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_DOWN)));

            EditText textPesoIdeal = findViewById(R.id.peso_ideal);
            textPesoIdeal.setText(pesoIdeal.toString());
            EditText textImc = findViewById(R.id.imc);

            BigDecimal imc = peso.divide(altura.pow(2),2, BigDecimal.ROUND_HALF_DOWN);
            textImc.setText(imc.toString());
            setInterpretacao(imc);
        });

        btnLimpar = findViewById(R.id.btn_limpar);
        btnLimpar.setOnClickListener(view -> {
            EditText textPesoIdeal = findViewById(R.id.peso_ideal);
            EditText textInterpretacao = findViewById(R.id.interpretacao);
            EditText textPeso = findViewById(R.id.peso);
            EditText textAltura= findViewById(R.id.altura);

            textPesoIdeal.setText("");
            textInterpretacao.setText("");
            textPeso.setText("");
            textAltura.setText("");

            EditText textImc = findViewById(R.id.imc);
            textImc.setText("");
        });
    }

    private void setInterpretacao(BigDecimal imc){
        EditText textInterpretacao = findViewById(R.id.interpretacao);
        if (imc.compareTo(BigDecimal.valueOf(20)) < 0) {
            textInterpretacao.setText("Baixo Peso");
        } else if (imc.compareTo(BigDecimal.valueOf(25)) < 0){
            textInterpretacao.setText("Normal");
        } else if (imc.compareTo(BigDecimal.valueOf(30)) <= 0){
            textInterpretacao.setText("Acima do peso");
        } else {
            textInterpretacao.setText("Obeso");
        }
    }
}
