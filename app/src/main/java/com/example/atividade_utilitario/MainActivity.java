package com.example.atividade_utilitario;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button btnImc;
    private Button btnConversao;
    private Button btnDisketGiga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnImc = findViewById(R.id.btn_imc);
        btnImc.setOnClickListener(o -> {
            Intent it = new Intent(MainActivity.this, ImcActivity.class);
            startActivity(it);
        });

        btnConversao = findViewById(R.id.btn_conversao);
        btnConversao.setOnClickListener(o -> {
            Intent it = new Intent(MainActivity.this, ConversaoDolarActivity.class);
            startActivity(it);
        });

        btnDisketGiga = findViewById(R.id.btn_disket_giga);
        btnDisketGiga.setOnClickListener(o -> {
            Intent it = new Intent(MainActivity.this, DisketGigaActivity.class);
            startActivity(it);
        });
    }
}
